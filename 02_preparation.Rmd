---
title: "Préparation des données"
output: 
  html_document: 
    number_sections: yes
---

```{r setup, echo=FALSE, cache=FALSE}
library(knitr)
library(rmdformats)

## Global options
options(max.print="75")
opts_chunk$set(echo=TRUE,
               cache=TRUE,
               prompt=FALSE,
               tidy=FALSE,
               comment=NA,
               message=FALSE,
               warning=FALSE, 
               eval=TRUE)
opts_knit$set(width=75)
```

Voici l'étape de préparation des données que nous utilisons dans le projet. 
Il s'agira d'importer le fichier des maternités, de les géocoder, d'importer les communes et départements de Bretagne etc.


# Sources mobilisées

## Les maternités

**Hélène, quelle est la source de ce fichier des maternités?**

Nous n'avons pas l'adresse précise de certaines maternité fermées.  

Modification du fichier Mater_bret.csv pour : 

- le CH de Laval, CP imprécis 53015 -> 53000
- CH Guingamp 19 Rue de l'Armor -> 	19 Rue de l'Armor 22200 Pabu 
- CENTRE HOSPITALIER LANNION	Venelle de Kergomar	Lannion	-> Rue Trorozec

## Les naissances

Naissances de 2009 à 2018    
page : https://www.insee.fr/fr/statistiques/1893255  
data : https://www.insee.fr/fr/statistiques/fichier/1893255/base_naissances_2018.zip  
Mise en ligne le 19/09/2019       Géographie au 01/01/2019   
Source(s) : Insee, Etat civil.

## La population selon le sexe

Population selon le sexe et l'âge quinquennal de 1968 à 2016  
page : https://www.insee.fr/fr/statistiques/1893204  
data : https://www.insee.fr/fr/statistiques/fichier/1893204/pop-sexe-age-quinquennal6816.zip  
Découpage géographique en géographie courante (en vigueur à la date des recensements)  
Source : Insee, Base historique des recensements de la population, exploitation complémentaire.    

## Les communes  
Admin express    
page : https://geoservices.ign.fr/documentation/diffusion/telechargement-donnees-libres.html#admin-express  
data : ftp://Admin_Express_ext:Dahnoh0eigheeFok@ftp3.ign.fr/ADMIN-EXPRESS-COG_2-0__SHP__FRA_L93_2019-09-24.7z.001  



# Import


## Les maternités


Pour géocolaliser les adresses des différentes maternités recensées, 
nous utiliserons la librarie de géocodage `banR`, 
qui repose sur [**la Base Adresses Nationale**](https://adresse.data.gouv.fr/)
constituée par plusieurs producteurs de données :  **La Poste**, l'**IGN**, la **DGFiP**, **OpenstreetMap France**, et la mission Etalab (service du Premier 
Ministre chargé de l'Open Data en France). Il s'agit de la 
**meilleure base de données (et donc du meilleur package) pour géocoder des adresses en France**.         


La fonction `geocode_tbl()` permet de récupérer une longitude et une latitude à partir d'une adresse postale. 
A partir des longitudes et latitudes récupérées, nous pouvons construire une couche géographique de points.


```{r}
maternite <- read.csv("data-raw/Mater_bret.csv", sep = ";")
# Formatage des adresses
maternite$Full_adresse <- paste0(maternite$ADRESSE, ", ", maternite$COMMUNE)
# GEOCODAGE des maternités
library(banR)
maternite <- geocode_tbl(maternite, 
                         adresse = Full_adresse, 
                         code_postal = CP)

library(sf)
#### transformer les coordonnées GEOCODAGE en fichier de point
# tranformation du tableau en objet SF (point) à partir des longitudes/latitudes
maternite <- st_as_sf(data.frame(maternite), coords = c("longitude", "latitude"), crs = 4326)
# Puis on ne garde que les variables pertinentes
maternite <- maternite[,c("ID", "NOM", "result_city","result_citycode", "NIVEAU", "FERME")]
colnames(maternite)[3:4] <- c("COMMUNE","INSEE_COM")
# 1) On applique la projection lambert 93 aux maternités (géotraitements !)
# lambert 93 -> epsg:2154 = Projection officielle francaise
maternite <- st_transform(maternite, crs = 2154)
# Creation d'une variable booleénne sur le statut (ouverte/fermée)
maternite$Fermeture <- ifelse(is.na(maternite$FERME), "Ouverte", "Fermée")
# Variable purement technique pour faciliter la représentation
maternite$var_temp <- 1
maternite <- maternite[order(maternite$Fermeture,decreasing = F),]


```

Nos maternités se présentent dorénavant sous la forme de points géolocalisés dans l’espace (couche géographique = objet sf).




## Les communes et les departements

Nous avons besoin des départements, des communes et des chefs lieux de communes

```{r}
# Chargament de la librairie sf
library(sf)
# Récupération de la couche géographique des communes françaises
com_fr <- st_read('data-raw/ADMIN-EXPRESS-COG_2-0__SHP__FRA_L93_2019-09-24.7z/ADMIN-EXPRESS-COG_2-0__SHP__FRA_2019-09-24/ADMIN-EXPRESS-COG/1_DONNEES_LIVRAISON_2019-09-24/ADE-COG_2-0_SHP_LAMB93_FR/COMMUNE_CARTO.shp', 
                  quiet = TRUE)
com_fr_cl <- st_read('data-raw/ADMIN-EXPRESS-COG_2-0__SHP__FRA_L93_2019-09-24.7z/ADMIN-EXPRESS-COG_2-0__SHP__FRA_2019-09-24/ADMIN-EXPRESS-COG/1_DONNEES_LIVRAISON_2019-09-24/ADE-COG_2-0_SHP_LAMB93_FR/CHEF_LIEU.shp', quiet = TRUE)

# Récupération de la couche géographique des départements français
dep_fr <- st_read('data-raw/ADMIN-EXPRESS-COG_2-0__SHP__FRA_L93_2019-09-24.7z/ADMIN-EXPRESS-COG_2-0__SHP__FRA_2019-09-24/ADMIN-EXPRESS-COG/1_DONNEES_LIVRAISON_2019-09-24/ADE-COG_2-0_SHP_LAMB93_FR/DEPARTEMENT_CARTO.shp', quiet=TRUE)
```

## La zone d'étude
La zone d'étude doit comprendre les departements de Bretagne + un buffer de 
25 km autour des maternités. 

```{r}
bbreg <- st_bbox(st_buffer(dep_fr[dep_fr$INSEE_REG == 53,], 5000))
bbmat <- st_bbox(st_buffer(maternite, 25000))
bbs <- rbind(bbreg, bbmat)
study_box <-  st_sf(
  st_as_sfc(
    st_bbox(
      c(xmin = min(bbs[,1]), ymin = min(bbs[,2]),
        xmax = max(bbs[,3]), ymax =  max(bbs[,4])), 
      crs = st_crs(2154)
    )
  )
)


```

```{r}
# 3) Découpage du fond de carte communale/departementale à partir de l'emprise calculée
com <- st_intersection(x = com_fr, y = study_box)
dep <- st_intersection(x = dep_fr, y = study_box)
```


## Les données communales INSEE

```{r}
library(readxl)
# Lecture du fichier INSEE naissances
data_nais <- read_excel("data-raw/base_naissances_2018.xls", 
                        sheet = "COM", 
                        skip = 5)
data_nais <-data.frame(INSEE_COM = data_nais$CODGEO, 
                       nais_0918 = round(rowSums(data_nais[5:14], na.rm = TRUE),0))
data_pop <- read_excel("data-raw/pop-sexe-age-quinquennal6816.xls", 
                       sheet = "COM_2016", 
                       skip = 13)
data_pop <- data.frame(INSEE_COM = paste0(data_pop$DR, data_pop$CR), 
                       fem_1549_16 = round(data_pop$ageq_rec04s2rpop2016 + 
                                             data_pop$ageq_rec05s2rpop2016 + 
                                             data_pop$ageq_rec06s2rpop2016 + 
                                             data_pop$ageq_rec07s2rpop2016 + 
                                             data_pop$ageq_rec08s2rpop2016 + 
                                             data_pop$ageq_rec09s2rpop2016 +
                                             data_pop$ageq_rec10s2rpop2016,0),
                       pop_2016 = round(rowSums(data_pop[7:46], na.rm = TRUE),0))

# merge des données dans com
com <- merge(com, data_pop, by = "INSEE_COM", all.x = TRUE)
com <- merge(com, data_nais, by = "INSEE_COM", all.x = TRUE)
# communes de la région
com_reg <- com[com$INSEE_REG == 53, ] 
# departement de la région
dep_reg <- dep[dep$INSEE_REG == 53, ]
# Chefs lieux de communes
com_cl <- com_fr_cl[com_fr_cl$INSEE_COM %in% com$INSEE_COM, ]
com_reg_cl <- com_fr_cl[com_fr_cl$INSEE_COM %in% com_reg$INSEE_COM,]

row.names(com) <- com$INSEE_COM
row.names(com_cl) <- com_cl$INSEE_COM
row.names(com_reg) <- com_reg$INSEE_COM
row.names(com_reg_cl) <- com_reg_cl$INSEE_COM
row.names(maternite) <- maternite$ID

```


# Les matrices de distances et les plus courts chemins

Préparation de données d'accessibilité
Nous allons calculer des matrices de distances à vol d'oiseau, des matrices de distance par la route. 
Nous allons aussi extraire les plus court chemin par la route via OSRM. 

## Matrices de distances

Nous allons créer plusieurs matrices de distances en utilisant plusieurs métriques :

- distances entre chef lieux de de communes (distance euclidienne et routière)
- distance entre chefs lieux de communes et maternités (distance routière)


### Distance euclidienne

#### Distance entre communes

```{r, eval = FALSE}
library(SpatialPosition)
# chef lieux à chefs lieux
mat_comcom_euc <- CreateDistMatrix(knownpts = com_cl, unknownpts = com_cl)
# m => km
mat_comcom_euc <- round(mat_comcom_euc/1000, 1)
saveRDS(mat_comcom_euc, "data/mat/comcom_euc.rds")
```

### Distance routière

#### Distances entre communes
```{r, eval = FALSE}
library(osrm)
# Connexion au serveur OSRM proposé par le RIATE
options(osrm.server = "http://188.xxx.xx.xxx:5000/", osrm.profile = "driving")
# calcul de la matrice de distance
mat <- osrmTable(src = com_cl, dst = com_cl, measure = "duration")
# Récupération du dataframe avec les durées
mat_comcom_road <- mat$durations
# On enregistre la matrice !
saveRDS(mat_comcom_road, file = "data/mat/comcom_road.rds")
```

#### Distance aux maternités
```{r, eval=FALSE}
library(osrm)
# calcul de la matrice de distance
mat <- osrmTable(src = com_reg_cl, dst = maternite, measure = "duration")
# Récupération du dataframe avec les durées
mat_com_regmat_road <- mat$durations
saveRDS(mat_com_regmat_road, file = "data/mat/com_regmat_road.rds")
```


#### Identifiant, distance et chemin vers la maternité la plus proche 
Par niveau et par période
Avant T0 
Après T1

##### identifiant et distance
La fonction `get_nearest_mat()` permet de récupérer l'identifiant de la maternité la plus proche (var = "id") et 
la distance à celle-ci (var = "dist"). 


```{r, eval = TRUE}
d <- readRDS("data/mat/com_regmat_road.rds")
get_nearest_mat <- function(d, mater, com, niv, include.closed, var){
  if(!include.closed){
    mater <- mater[mater$Fermeture=="Ouverte",]
  }else{
  }
  mater <- mater[mater$NIVEAU %in% niv, ]
  j <- factor(mater$ID, levels =  mater$ID)
  i <- factor(com$INSEE_COM, levels =  com$INSEE_COM)
  d_target <- d[levels(i), levels(j)]
  
  if(var == "id"){
    out <- colnames(d_target)[apply(d_target, 1, which.min)]
  }
  if(var == "dist"){
    out <- apply(d_target, 1, min)  
  }
  out
}

com_reg_cl$T0_123_ID <- get_nearest_mat(d = d, mater = maternite, 
                                        com = com_reg_cl, niv = c(1,2,3),
                                        include.closed = TRUE, var = "id")
com_reg_cl$T0_123_D <- get_nearest_mat(d = d, mater = maternite, 
                                       com = com_reg_cl, niv = c(1,2,3),
                                       include.closed = TRUE, var = "dist")
com_reg_cl$T0_23_ID <- get_nearest_mat(d = d, mater = maternite, 
                                       com = com_reg_cl, niv = c(2,3),
                                       include.closed = TRUE, var = "id")
com_reg_cl$T0_23_D <- get_nearest_mat(d = d, mater = maternite, 
                                      com = com_reg_cl, niv = c(2,3),
                                      include.closed = TRUE, var = "dist")
com_reg_cl$T0_3_ID <- get_nearest_mat(d = d, mater = maternite, 
                                      com = com_reg_cl, niv = c(3),
                                      include.closed = TRUE, var = "id")
com_reg_cl$T0_3_D <- get_nearest_mat(d = d, mater = maternite, 
                                     com = com_reg_cl, niv = c(3),
                                     include.closed = TRUE, var = "dist")
com_reg_cl$T1_123_ID <- get_nearest_mat(d = d, mater = maternite, 
                                        com = com_reg_cl, niv = c(1,2,3),
                                        include.closed = FALSE, var = "id")
com_reg_cl$T1_123_D <- get_nearest_mat(d = d, mater = maternite, 
                                       com = com_reg_cl, niv = c(1,2,3),
                                       include.closed = FALSE, var = "dist")
com_reg_cl$T1_23_ID <- get_nearest_mat(d = d, mater = maternite, 
                                       com = com_reg_cl, niv = c(2,3),
                                       include.closed = FALSE, var = "id")
com_reg_cl$T1_23_D <- get_nearest_mat(d = d, mater = maternite, 
                                      com = com_reg_cl, niv = c(2,3),
                                      include.closed = FALSE, var = "dist")
com_reg_cl$T1_3_ID <- get_nearest_mat(d = d, mater = maternite, 
                                      com = com_reg_cl, niv = c(3),
                                      include.closed = FALSE, var = "id")
com_reg_cl$T1_3_D <- get_nearest_mat(d = d, mater = maternite, 
                                     com = com_reg_cl, niv = c(3),
                                     include.closed = FALSE, var = "dist")
```

##### Plus court chemin

La fonction `get_roads_mat()` exporte le chemin de tous les chefs lieux de communes vers la maternité la plus proche. 

```{r, eval = FALSE}
get_roads_mat <- function(com, materid, mater){
  l <- list()
  for (i in seq_len(nrow(com))){
    l[[i]] <- osrmRoute(com[i,], 
                        mater[mater$ID == com[[materid]][i],],
                        returnclass = "sf", overview = "full")
    print(round(100 * i / nrow(com),0))
  }
  road <- do.call(rbind,l)
  saveRDS(object = road, file = paste0("data/roads/",materid,".rds"))
}

get_roads_mat(com = com_reg_cl, materid = "T0_123_ID", mater = maternite)
get_roads_mat(com = com_reg_cl, materid = "T0_23_ID", mater = maternite)
get_roads_mat(com = com_reg_cl, materid = "T0_3_ID", mater = maternite)
get_roads_mat(com = com_reg_cl, materid = "T1_123_ID", mater = maternite)
get_roads_mat(com = com_reg_cl, materid = "T1_23_ID", mater = maternite)
get_roads_mat(com = com_reg_cl, materid = "T1_3_ID", mater = maternite)

```

Les indicateurs sont calculés sur les chefs lieux de communes, il faut récupérer cette information 
à la commune (polygones). 
```{r}
com_reg <- merge(
  x = com_reg, 
  y = com_reg_cl[,c("INSEE_COM",
                    "T0_123_ID","T0_23_ID", "T0_3_ID",
                    "T0_123_D","T0_23_D", "T0_3_D",
                    "T1_123_ID","T1_23_ID", "T1_3_ID",
                    "T1_123_D","T1_23_D", "T1_3_D"),drop=TRUE],
  by = "INSEE_COM", 
  all.x = TRUE
)


```


# Export des données
Sauvegarder l'ensemble des données utiles dans un seul et même fichier [geopackage](https://www.geopackage.org/). 

```{r}
# Les départements de la zone d'étude 
st_write(dep, dsn = "data/layers.gpkg", layer = "dep", quiet = TRUE, 
         delete_layer = TRUE)
# Les départements de Bretagne
st_write(dep_reg, dsn = "data/layers.gpkg", layer = "dep_reg", quiet = TRUE, 
         delete_layer = TRUE)
# Les communes de la zone d'étude
st_write(com, dsn = "data/layers.gpkg", layer = "com", quiet = TRUE, 
         delete_layer = TRUE)
# les chefs lieux de la zone d'étude
st_write(com_cl, dsn = "data/layers.gpkg", layer = "com_cl", quiet = TRUE, 
         delete_layer = TRUE)
# Les communes de Bretagne
st_write(com_reg, dsn = "data/layers.gpkg", layer = "com_reg", quiet = TRUE, 
         delete_layer = TRUE)
# Les chefs lieux des communes de Bretagne
st_write(com_reg_cl, dsn = "data/layers.gpkg", layer = "com_reg_cl", quiet = TRUE, 
         delete_layer = TRUE)
# La zone d'étude
st_write(study_box, dsn = "data/layers.gpkg", layer = "study_box", 
         quiet = TRUE, delete_layer = TRUE)
# Les maternités
st_write(maternite, dsn = "data/layers.gpkg", layer = "maternite", 
         quiet = TRUE, delete_layer = TRUE)
```

# Situation
```{r, results=FALSE}
library(cartography)
png("fig/situation.png", width = 910, height = 512, res = 100)
par(mar=c(0,0,1.2,0), xaxs =  'i', yaxs = 'i')
ghostLayer(study_box)
plot(st_geometry(com), border="grey60", lwd=0.2, add=TRUE)
plot(st_geometry(dep), border="grey30", lwd=0.5, add=TRUE)
plot(st_geometry(maternite), bg="darkslategray4", pch=23,cex = 1.5, add = TRUE)
layoutLayer("Maternités accessibles depuis la Bretagne", scale = 20, 
            sources = "IGN", author = "La team", tabtitle = TRUE)
dev.off()
```

![](fig/situation.png)

**Fin de la préparation des données, on peut commencer le programme à partir d'ici...** 


### Utils


```{r, echo = FALSE, eval = FALSE}
png("fig/gridp.png", width = 910, height = 512, res = 100)
par(mar=c(0,0,1.2,0), xaxs =  'i', yaxs = 'i')
ghostLayer(study_box, bg = "lightblue")
plot(st_geometry(dep_reg), col="grey90", border=NA, add=TRUE)
plot(st_geometry(dep), border="grey30", lwd=0.5, add=TRUE)
bb <- st_bbox(study_box)
xx <- seq(round(bb[1],-3), round(bb[3],-3), by = 5000)
abline(v = xx, lwd = .2)
yy <- seq(round(bb[2],-3), round(bb[4],-3), by = 5000)
abline(h = yy, lwd = .2)
axis(side = 1, at = xx, labels = xx/1000, line = -2, cex.axis = .5, padj=-6 )
axis(side = 2, at = yy, labels = yy/1000, line = -2, cex.axis = .5, padj=6)
dev.off()
```